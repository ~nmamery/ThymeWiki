*Back to [[Systems]] / WhiteWolf / WhiteWolf/Worlds / MagickalEarth*

----

**High Wizard Sarah Amery**

----

[http://www.ysolde.ucam.org/wiki/shadow?MagickalEarthHighWizardSarah Main] / [http://www.ysolde.ucam.org/wiki/shadow?MagickalEarthHighWizardSarah/Wizardry Wizardry] / [http://www.ysolde.ucam.org/wiki/shadow?MagickalEarthHighWizardSarah/BodyMorph Elemental Form] / [http://www.ysolde.ucam.org/wiki/shadow?MagickalEarthHighWizardSarah/Notes Notes] / [http://www.ysolde.ucam.org/wiki/shadow?MagickalEarthHighWizardSarah/Story Story]

----

|   |   |   |   |
|---|---|---|---|
|One|Two|Six|Ten|

foo

| X |   |   |   |
|---|---|---|---|
|One|Two|Six|Ten|

foo

| X |   |   |   |
|---|---|---|---|
|One|Two|Eleven||

foo

| X |   |   |   |
|---|---|---|---|
|One|Two|Eleven||
|<div style="text-align: center">Fourteen</div>||||

foo

| &nbsp; |   |   |   |
|---|---|---|---|
|One|Two|Eleven||

foo


*test*
**test**
_test_
__test__
*__test__*
_**test**_


|X| | | | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|-|-|-|
|**Spirtual Attributes**|||||||||||||


|*Willpower* | &#9679;&#9679;&#9679;&#9679;&#9679; &#9679;&#9679;|(7)|  | |*Magick*|&#9679;&#9679;&#9679;&#9679;|(4)|  | |*Resonance*|&#9679;|(1)|  |
| |
|***Attributes***|
|**Physical**|  | |  | |**Social**|  | |  | |**Mental**|  |
|*Strength*|&#9679;&#9679;|(2)|  | |*Charisma*|&#9679;&#9679;|(2)|  | |*Intelligence*|&#9679;&#9679;&#9679;&#9679;|(4)| Patterns |
|*Dexterity*|&#9679;&#9679;|(2)|  | |*Manipulation*|&#9679;&#9679;&#9679;&#9679;|(4)| Soothing|  |*Wits*|&#9679;&#9679;&#9679;|(3)|  |
|*Stamina*|&#9679;&#9679;|(2)|  | |*Appearance*|&#9679;&#9679;|(2)|  |  |*Perception*|&#9679;&#9679;&#9679;|(3)|  |
|  |
|  |
|***Abilities***| 
|  |
|**Strength**|  | |  | |**Dexterity**|  | |  | |**Stamina**|  |
|*Brawl*|&#9679;|(1)|  | |*Athletics*|  |(0)|  | |*Endurance*|  |(0)|  |
|*Might*|  |(0)|  | |*Drive*|  |(0)|  | |*Resistance*|&#9679;&#9679; |(2)|  |
|  | |  | |  |*Firearms*|&#9679;&#9679;|(2)|  | |  | |  | |
|  | |  | |  |*Legerdemain*|  |(0)|  | |  | |  | |
|  | |  | |  |*Martial Arts*|  |(0)|  | |  | |  | |
|  | |  | |  |*Melee*|  |(0)|  | |  | |  | |
|  | |  | |  |*Pilot*|  |(0)|  | |  | |  | |
|  | |  | |  |*Stealth*|  |(0)|  | |  | |  | |
|  |
|**Perception**|  | |  | |**Intelligence**|  | |  | |**Wits**|  |
|*Awareness*|&#9679;&#9679;|(2)|  | |*Academics*|&#9679;&#9679;|(2)|  | |*Arts*|&#9679; |(1)|  |
|*Investigation*|&#9679;&#9679;&#9679;|(3)|  | |*Bureaucracy*|&#9679;|(1)|  | |*Biz*|&#9679; |(1)|  |
|  | |  | |  |*Computer*|&#9679;|(1)|  | |*Rapport*|&#9679;&#9679;&#9679; |(3)|  |
|  | |  | |  |*Engineering*|&#9679;|(1)|  | |  | |  | |
|  | |  | |  |*Intrusion*|&#9679;|(1)|  | |  | |  | |
|  | |  | |  |*Linguistics*|&#9679;|(1)|  | |  | |  | |
|  | |  | |  |*Medicine*|&#9679;|(1)|  | |  | |  | |
|  | |  | |  |*Science*|&#9679;|(1)|  | |  | |  | |
|  | |  | |  |*Survival*|&#9679;|(1)|  | |  | |  | |
|  |
|**Appearance**|  | |  | |**Manipulation**|  | |  | |**Charisma**|  |
|*Intimidation*|  |(0)|  | |*Interrogation*|  |(0)|  | |*Command*|  |(0)|  |
|*Style*|  |(0)|  | |*Streetwise*|  |(0)|  | |*Etiquette*|&#9679;&#9679; |(2)|  |
|  | |  | |  |*Subterfuge*|  |(0)|  | |*Perform*|&#9679; |(1)|  |
|  |
|  |
|***Backgrounds***| 
|Allies|  |(0)|  | |Contacts|&#9679;&#9679;|(2)|The two wizards|  |Mentor|&#9679;&#9679;|(2)|*Girion*|
|Attunement|&#9679;&#9679;&#9679;|(3)|  | |Followers|  |(0)|  | |Resources|&#9679;&#9679; |(2)|
|Backing|&#9679;&#9679; |(2)|Interpol M |  |Influence|  |(0)|  | |Talent|&#9679;&#9679;&#9679;  |(3)|
|Cipher|&#9679;|(1)|  | |Magickal Material|  |(0)|  | |  | |  |




