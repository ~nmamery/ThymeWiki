|    | Lunch                  | Dinner  
|---:|:-----------------------|:--------
 Wed | Gousto*                |  Pie*
 Thu | Sammich                |  Lamb Biryani
 Fri | Sammich                |  Gousto*
 Sat | (Town)                 |  Gousto*
 Sun | Gousto                 |  Pie
 Mon | Sammich                |  Pizza* / S Pasta
 Tue | Sammich                |  Leftovers?


 Items marked with a * have been purchased.
