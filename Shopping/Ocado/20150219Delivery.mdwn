|    | Lunch                  | Dinner  
|---:|:-----------------------|:--------
 Thu | FOOD                   |  Sammich
 Fri | Sammich                |  Gousto* (Schnitzel?)
 Sat | BRIDGE: Sammiches      |  Gousto* (Jerk Chicken?)
 Sun | Gousto* (Risotto?)     |  Pie
 Mon | Sammich                |  Pizza* / S Pasta
 Tue | Sammich                |  Scampi & Chips
 Wed | Food                   |  Pie



 Items marked with a * have been purchased.
