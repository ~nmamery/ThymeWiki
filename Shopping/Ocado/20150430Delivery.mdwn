|    | Lunch                  | Dinner  
|---:|:-----------------------|:--------
 Thu | Sammich                |  Kedgeree
 Fri | Sammich                |  Gousto*
 Sat | Sammich                |  Gousto*
 Sun | Town                   |  Gousto*
 Mon | Sammich                |  Pizza* / S Pasta
 Tue | Sammich                |  Scampi
 Wed | Sammich                |  Lamb and Couscous


 Items marked with a * have been purchased.
