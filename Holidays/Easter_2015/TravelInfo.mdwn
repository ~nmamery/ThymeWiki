**Itinerary**

| From | Get | To | Time 1 | Time 2 | Notes |
|:----|:----|:----|:----|:----|:----|
| &nbsp; ||||||

| Home | Taxi | CBG | 18:30 | 18:45 | Jon booking |
| CBG | Train | KGX | 19:15 | 20:06 | If happen to get there for earlier train, get that |
| KGX | Walk | Hotel | ~20:06 | ~20:20 |  |
| &nbsp; ||||||

| Stay at [[Kings Cross Inn Hotel|http://www.kingscrossinnhotel.co.uk/]] ||| 25 March | 26 March | Directly opposite KGX|
| &nbsp; ||||||

| Hotel | Travel | EUS | 04:45 | 05:20 | Check travel options on day or walk |
| EUS | Train | LIV | 05:26 | 08:05 | Seats H01 and H03 |
| LIV | Taxi | Birkenhead | ~08:10 | ~08:30 | Skelhorne Street entrance for taxis. Merseycabs: 0151 298 2222 |
| Birkenhead | [[Stena Lagan ferry|http://www.stenaline.co.uk/ferries-to-ireland]] | Belfast | 10:30 | 18:30 | Check in between 08:30 and 09:30 latest. Booking reference 76634342|
| &nbsp; ||||||

| Stena Line Terminus | Bus 96 | Upper Queen Street | 19:05 | 19:25 | Bus towards Belfast City Center |
| Upper Queen Street | Walk | Howard Street | 19:25 | 19:30 | Walk to the end of the street, cross over and turn left |
| Howard Street | Bus 7D | Shaftesbury Square on Botanic Avenue | 19:30 | 19:33 | Bus towards Cairnshill Laurel Grove |
| Shaftesbury Square on Botanic Avenue | Walk | Botanic Train Station | 19:33 | 19:36 |  |
| Botanic Station | Train | Helen's Bay Train Station | 19:36 | 20:03 | Train to Bangor. Later Trains Available. |
| Helen's Bay Train Station | Walk | Hotel | 20:03 | 20:33 | Fallback plan: [[Bangor Cabs|http://www.bangorcabs.com/]] Have App|
| &nbsp; ||||||

| Stay at [[The Old Inn Crawfordsburn|http://www.theoldinn.com/]] ||| 26 March | 1 April | Checkout by 11:00 |
| &nbsp; ||||||

| Hotel | Walk | Helen's Bay Train Station | ~11:15 | ~11:45 | Fallback plan: [[Bangor Cabs|http://www.bangorcabs.com/]] Have App |
| Helen's Bay Train Station | Train | Great Victoria Street | 12:06 | 12:37 | Trains every half hour, and can stop sooner if desired. |
| Afternoon in Belfast ||| 1 April | 1 April |  |
| Belfast | Taxi | Stena line Terminus | ~20:15 | ~20:30 |  |
| Stena line Terminus | [[Stena Mersey ferry|http://www.stenaline.co.uk/ferries-to-ireland]] | Birkenhead | 22:30 | 06:30 | Check in between 20:30 and 21:30. Booking reference 76634342 |
| &nbsp; ||||||

| Birkenhead | Taxi | LIV | ~06:30 | ~07:00 | Merseycabs: 0151 298 2222 |
| Breakfast ||| 07:00 | 09:00 | Somewhere near LIV |
| LIV | Train | EUS | 09:47 | 11:59 | Seats H1 and H3 |
| EUS | Bus | KGX | ~12:15 | ~12:30 |  |
| Lunch ||| ~12:30 | ~13:00 | Somewhere in KGX |
| KGX | Tube | Heathrow T1/2/3 | ~13:30 | ~14:30 | Picadilly Line |
| Heathrow T1/2/3 | Hotel Hoppa | Hotel | ~14:30 | ~15:00 |  |
| &nbsp; ||||||

| Stay at [[The Park Inn Hotel Heathrow|http://www.parkinn.co.uk/airporthotel-heathrow]] ||| 2 April | 6 April | Confirmation number: H4H9DCQ |
| &nbsp; ||||||

| Hotel  | Hotel Hoppa |  Heathrow T1/2/3 | ~17:00 | ~17:30 |  |
| Heathrow T1/2/3 | Tube | KGX | ~17:30 | ~18:30 | Picadilly line |
| KGX | Train | CBG | 18:44 | 19:30 |  |
| CBG | Taxi | Home | 19:35 | 19:50 |  |
| &nbsp; ||||||

<a name="TravelDocs"></a>
__Travel Documents__

* Booking References for:
  * [[Kings Cross Inn Hotel|http://www.kingscrossinnhotel.co.uk/]]
  * [[Stena Line Ferries|http://www.stenaline.co.uk/ferries-to-ireland]]
  * [[The Park Inn Hotel Heathrow|http://www.parkinn.co.uk/airporthotel-heathrow]]

* Train Tickets for:
  * CBG to London Terminals x2
  * Reservation from EUS to LIV x2
  * Ticket from London Terminals to Liverpool Stations
  * Reservation from LIV to EUS x2
  * Ticket from Liverpool Stations to London Terminals x2
  * London Terminals to CBG x2





